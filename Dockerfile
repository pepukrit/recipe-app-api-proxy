# Create a base image, here we use unprivilege image that's not specific to root user
FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="pepukrit@hotmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root
# swap to root account because we're going to do privileged tasks in linux

RUN mkdir -p /vol/static
# create vol and static accordingly. Vol is not created yet, so -p would help doing that
RUN chmod 755 /vol/static
# allow permission /vol/static path to be only read and execute for users
RUN touch /etc/nginx/conf.d/default.conf
# create empty file of default.conf. Without the existence of the file, envsubst will not work
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
# change ownership to be user nginx and group nginx for default.conf
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
# In linux, .sh files are not executable. Chmod +x would allow it to do so

USER nginx
# swap to nginx user before running .sh file

CMD ["/entrypoint.sh"]
