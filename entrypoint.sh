#!/bin/sh

set -e 
# By setting -e, it expects all script command run successfully. If not being so, it will stop immediately which easily help debugging

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf 
# Substitute nginx config file with env_variables in the new file named default.conf

nginx -g 'daemon off;' 
# Following the official documentation, docker is supposed to run on foreground task.
# By default, docker runs in background task.  By setting daemon off, it will run nginx on foreground task